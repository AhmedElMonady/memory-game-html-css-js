var imgs_lvl1 = ["wanda", "ant-man", "black-panther-2", "black-panther",  "black-widow-2", 
"black-widow-png-20494", "bucky-2", "bucky", "wanda", "ant-man", "black-panther-2", "black-panther",
"black-widow-2", "black-widow-png-20494", "bucky-2", "bucky"];

var imgs_lvl2 = ["wanda", "ant-man", "black-panther-2", "black-panther","black-widow-2", 
"black-widow-png-20494", "bucky-2", "bucky", "captain-america-2", "captain-america", "wanda", 
"ant-man", "black-panther-2", "black-panther", "black-widow-2", "black-widow-png-20494", "bucky-2", 
"bucky", "captain-america-2", "captain-america"];

var imgs_lvl3 = ["wanda", "ant-man", "black-panther-2", "black-panther", "black-widow-2", 
"black-widow-png-20494", "bucky-2", "bucky", "captain-america-2", "captain-america", "dr-strange-2", 
"dr-strange", "gamora-2", "gamora", "hawkeye-2", "hawkeye-png-21577", "hulk-2", "Hulk","wanda", 
"ant-man", "black-panther-2", "black-panther", "black-widow-2", "black-widow-png-20494", "bucky-2", 
"bucky", "captain-america-2", "captain-america", "dr-strange-2", "dr-strange",  "gamora-2", "gamora", 
"hawkeye-2", "hawkeye-png-21577", "hulk-2", "Hulk" ];


function index_load(){
    var p1_img_check = document.getElementById("p1_img_check");
    var p2_img_check = document.getElementById("p2_img_check");
    var p1_img_set = document.getElementById("p1_img_set");
    var p2_img_set = document.getElementById("p2_img_set");

    p1_img_check.addEventListener("change", function(){
        if(p1_img_check.checked){
            sessionStorage.p1_checked = true;
            var xhr = new XMLHttpRequest();

            xhr.open("get","partial_views/player_image.html");

            xhr.send();

            xhr.onload = function() {
                if(this.status === 200){
                    p1_img_set.innerHTML = xhr.responseText;
                    var i;
                    for(i=1;i<=9;i++){
                        $("#"+i).attr("alt",i);
                        $("#"+i).attr("id","p1_"+i);
                    }
                }
            };
        }
        else{
            sessionStorage.p1_checked = false;
            $("#p1_img_set").empty();
        }
    });

    p2_img_check.addEventListener("change", function(){
        if(p2_img_check.checked){
            sessionStorage.p2_checked = true;
            var xhr = new XMLHttpRequest();

            xhr.open("get","partial_views/player_image.html");

            xhr.send();

            xhr.onload = function() {
                if(this.status === 200){
                    p2_img_set.innerHTML = xhr.responseText;
                    var i;
                    for(i=1;i<=9;i++){
                        $("#"+i).attr("alt",i);
                        $("#"+i).attr("id","p2_"+i);
                    }
                }
            };
        }
        else{
            sessionStorage.p2_checked = false;
            $("#p2_img_set").empty();
        }
    });
}



function startGame(){
    var p1 = document.getElementById("p1name").value;
    var p2 = document.getElementById("p2name").value;
    if(p1.trim() != "" && p2.trim() != ""){
    var level = document.querySelector('input[name="select_level"]:checked').value;
    if(!(sessionStorage.p1_checked === true)){
        sessionStorage.p1_img = generate_random_number(9) + 1; //generates 1 -> 9
    }
    if(!(sessionStorage.p2_checked === true)){
        sessionStorage.p2_img = generate_random_number(9) + 1; //generates 1 -> 9
    }
    sessionStorage.p1 = p1;
    sessionStorage.p2 = p2;
    sessionStorage.level = level;
    
    sessionStorage.StartGame = "start";

    window.location.href = "level"+".html";
    }
    else{
        alert("One or more fields are left empty!.");
    }
}

function load_game_header(){
    if(sessionStorage.StartGame == "start"){
        var xhr = new XMLHttpRequest();
        var game_header = document.getElementById("game_header");

        xhr.open('get',"partial_views/game_header.html");

        xhr.send();

        xhr.onload = function(){
            if(this.status === 200){
                game_header.innerHTML = xhr.responseText;
                onLevelLoading();
                updatePageTitles();
                avoid_img_drag_and_drop();
            }
        };
    }
    else{
        window.location.href = "index.html";
    }
}

function updatePageTitles(){
    var level = sessionStorage.level;
    var page_title = document.getElementById("page_title");
    var level_title = document.getElementById("level_title");
    var text = "Level " + level;
    level_title.textContent = text;
    page_title.textContent = text;
}

function onLevelLoading(){

    document.getElementById("player_1_float").classList.add("current_player");

    var p1_name = document.getElementById("p1namedisplay");
    var p1_score = document.getElementById("p1scoredisplay");
    var p1_image = document.getElementById("p1image");
    
    var p2_name = document.getElementById("p2namedisplay");
    var p2_score = document.getElementById("p2scoredisplay");
    var p2_image = document.getElementById("p2image");
    p1_name.textContent = sessionStorage.p1;
    p1_score.textContent = 0;
    var img_path = "img/player/"+sessionStorage.p1_img+".png";
    p1_image.setAttribute('src',img_path);
    
    p2_name.textContent = sessionStorage.p2;
    p2_score.textContent = 0;
    var img_path = "img/player/"+sessionStorage.p2_img+".png";
    p2_image.setAttribute('src',img_path);
}

function generate_random_number(max){
    //to get range from 0 -> max, pass by the max+1
    return Math.floor((Math.random() * (max)));
}

function shuffle_array(image_array){
    var currentIndex = image_array.length;
    var tempVal, randomIndex;

    while(currentIndex !== 0){
        randomIndex = generate_random_number(currentIndex);
        currentIndex -= 1;
        tempVal = image_array[currentIndex];
        image_array[currentIndex] = image_array[randomIndex];
        image_array[randomIndex] = tempVal;
    }
    return image_array;
}

var numOfClicks=0;
var firstClick;
var secondClick;
var playersTurn=1;
var temp;

function checkImage(a){
    var index=a.children;
    var x=index[1];
    var y=index[0];
    console.log(x.src);
    console.log(y.src);
    if(x.alt==="clicked")
    {
        console.log("clicked");
    }
    else if(numOfClicks===0)
    {
        numOfClicks+=1;
        temp=x;
        x.alt="clicked";
        console.log(x.src);
        x.src=y.src;
        firstClick=y.alt;
        //console.log(x.src);
        console.log(y.src);
        console.log(firstClick);
        console.log("else if");
    }
    else
    {
        x.src=y.src;
        temp.alt="notclicked";
        console.log('else');
        secondClick=y.alt;
        numOfClicks=0;
        if(firstClick===secondClick)
        {
            incrementPlayer(playersTurn);
            setTimeout(()=>{
                changePlayer();
            },500);
            temp.alt="clicked";
            x.alt="clicked";
        }
        else
        {
            setTimeout(()=>{
                changePlayer();
            },150);
            setTimeout(()=>{
                x.src="img/logo.png";
                temp.src="img/logo.png";
            },500);
        }
    }
    GameOver();
}

function incrementPlayer(a){
    if(a===1)
    {
        var p1_score = document.getElementById("p1scoredisplay");
        var p1_i = parseInt(p1_score.textContent);
        p1_i += 2;
        p1_score.textContent = p1_i; 
		sessionStorage.p1_score = p1_i;
    }
    else
    {
        var p2_score = document.getElementById("p2scoredisplay");
            var p2_i = parseInt(p2_score.textContent);
            p2_i += 2;
            p2_score.textContent = p2_i;
			sessionStorage.p2_score = p2_i;
    }
}

function changePlayer(){
    if(playersTurn===1)
    {
        document.getElementById("player_1_float").classList.remove("current_player");
        document.getElementById("player_2_float").classList.add("current_player");
        playersTurn=2;
    }
    else
    {
        document.getElementById("player_2_float").classList.remove("current_player");
        document.getElementById("player_1_float").classList.add("current_player");
        playersTurn=1;
    }
}

function GameOver(){
    console.log("entered function");
    var level=sessionStorage.level;
    console.log(level);
    var score;
    var p1_score = document.getElementById("p1scoredisplay");
    var p1_i = parseInt(p1_score.textContent);
    var p2_score = document.getElementById("p2scoredisplay");
    var p2_i = parseInt(p2_score.textContent);
    if(level==1)
    {
        score=16;
    }
    else if(level==2)
    {
        score=20;
    }
    else
    {
        score=36;
    }
    console.log(p1_i);
    console.log(p2_i);
    var calc=p2_i+p1_i;
    console.log(calc);
    console.log(score);
    if(calc===score)
    {
        console.log("score is reached");    
        if(p1_i>p2_i)
        {
            setTimeout(()=>{ 
                document.getElementById("player_1_float").style.backgroundColor= "yellow";
                document.getElementById("player_2_float").style.backgroundColor= "yellow";
                alert("Player 1 wins");
                document.getElementById("gamebody").style.backgroundImage="url(img/gameover/congrats.png)";
                $("#gamebody").empty();
            },100);
        }
        else if(p2_i>p1_i)
        {
            setTimeout(()=>{ 
                document.getElementById("player_1_float").style.backgroundColor= "yellow";
                document.getElementById("player_2_float").style.backgroundColor= "yellow";
                alert("Player 2 wins");
                document.getElementById("gamebody").style.backgroundImage="url(img/gameover/congrats.png)";
                $("#gamebody").empty();
            },100);
        }
        else
        {
            setTimeout(()=>{ 
                document.getElementById("player_1_float").style.backgroundColor= "yellow";
                document.getElementById("player_2_float").style.backgroundColor= "yellow";
                alert("It is a draw goodluck next time");
                document.getElementById("gamebody").style.backgroundImage="url(img/gameover/sad.png)";
                document.getElementById("gamebody").style.backgroundPosition="center";
                document.getElementById("gamebody").style.backgroundRepeat="no-repeat";
                $("#gamebody").empty();
            },100);
        }
        sessionStorage.StartGame = "end";
        sessionStorage.clear();
    }
}

function security_guards(){
        //prevent inspect via keys
    $(document).keydown(function(e){
        
            if(e.keyCode == 123) {
        return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
        return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
        return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
        return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
        return false;
    }
    if(e.ctrlKey && e.keyCode == "S".charCodeAt(0)){
        return false;
    }


    });
        //prevent image drag and drop
    avoid_img_drag_and_drop();
        //prevent open context menu
    $("body").on("contextmenu", "img", function(e) {
    return false;
    });
    $("body").on("contextmenu", function(e){
        return false;
    });
}

function avoid_img_drag_and_drop(){
    $('img').on('dragstart', function(event) { event.preventDefault(); });
}

function generate_card_div(i, path, alt){
    var s;
    var level = sessionStorage.level;
    s = '\t<div class="cardbox_level'+sessionStorage.level+'" onclick="checkImage(this)" >\n';
    s += '\t\t<img src='+path+' alt='+alt+' class="img1" id="card_img_"'+i+'/>\n';
    s += '\t\t<img src = "img/logo.png" class="img2" id="logo'+i+'"  />\n';
    s += '\t</div>\n';
    return s;
}

function generate_cards_box(){
    var shuffled;
    var level = sessionStorage.level;
    if(level == 1){
        shuffled = shuffle_array(shuffle_array(imgs_lvl1));
    }
    else if(level == 2){
        shuffled = shuffle_array(shuffle_array(imgs_lvl2));
    }
    else if(level == 3){
        shuffled = shuffle_array(shuffle_array(imgs_lvl3));
    }
    var i,path,img_root,alt;
    var s = "";
    img_root = "img/cards/";
    for(i = 0;i<shuffled.length;i++){
        path = img_root + shuffled[i] + ".png";
        alt = shuffled[i];
        s += generate_card_div((i+1),path, alt);
    }
    var el = document.getElementById("gamebody");
    el.classList.add("game_box_level"+level);
    el.innerHTML = '\n' + s + '\n';
}

function custom_player_image_click(current_div){
    var parent = current_div.parentNode.id;
    var child = current_div.children[0].alt;
    if(parent == "p1_img_set"){
        sessionStorage.p1_img = child;
        var i = 0;
        for(i=1;i<=9;i++){
            $("#p1_"+i).removeClass("img_selected");
        }
        $("#p1_"+child).addClass("img_selected");
    }
    else{
        sessionStorage.p2_img = child;
        var i = 0;
        for(i=1;i<=9;i++){
            $("#p2_"+i).removeClass("img_selected");
        }
        $("#p2_"+child).addClass("img_selected");
    }
}